import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.PatternSyntaxException;

/**
 * <p>Classe permettant la lecture de fichier LIF</p>
 * 
 * <p><b>Note: </b> En l'absence de précision nous avons décider qu'un fichier LIF
 * pour les mondes circulaires et fermé posséderait la ligne correspondant ou
 * les quatre valeurs suivant le tag seraient les limites du monde</p>
 * <p>#S ymin ymax xmin xmax pour monde fermé</p>
 * <p>#T ymin ymax xmin xmax pour monde circulaire</p>
 * 
 * @author NAIL Theo
 *
 */
public class LecteurLif {
	private File f;
	private InputStream ips;
	private InputStreamReader ipsr;
	private BufferedReader br;
	private String s;
	private int[] survie;
	private int[] naissance;
	private int[] limites;
	// 0 pour le monde normal 1 pour le monde limité 8 pour le monde circulaire
	// Dans le fichier lif
	// #S ymin ymax xmin xmax pour monde fermé
	// #T ymin ymax xmin xmax pour monde circulaire
	private int type;

	/**
	 * Créer un licteur de fichier LIF met a jour les variables nécèssaire a la
	 * récupération des différentes information de ces monde
	 * 
	 * @param F
	 *            Un fichier LIF
	 * @throws IOException
	 * 				Si l'un des arguments n'est pas un nombre
	 * 
	 * @throws PatternSyntaxException
	 *             Si le fichier LIF est mal formater
	 */
	public LecteurLif(File F) throws IOException, PatternSyntaxException {
		this.f = F;
		this.ips = new FileInputStream(f);
		this.ipsr = new InputStreamReader(ips);
		this.br = new BufferedReader(ipsr);
		this.s = "";
		String s2 = "";
		this.br.readLine();
		int compteurcomentaire = 0;
		this.type = 0;
		while ((s2 = this.br.readLine()) != null) {
			s2 = s2.trim();
			if (s2.startsWith("#D ")) {
				compteurcomentaire++;
				if(compteurcomentaire>22){
					throw new PatternSyntaxException("Trop de commentaires dans ce fichier:" + this.f.getAbsolutePath(), s2, -1);
				}
			} else if (s2.startsWith("#N")) {
				int s[] = { 2, 3 };
				this.survie = s;
				int n[] = { 3 };
				this.naissance = n;
			} else if (s2.startsWith("#R ")) {
				if (s2.matches("#R\\s\\d+/\\d+")) {
					s2 = s2.substring(3);
					String s3[] = s2.split("/");
					this.survie = new int[s3[0].length()];
					this.naissance = new int[s3[1].length()];
					for (int i = 0; i < s3[0].length(); i++) {
						this.survie[i] = Integer.parseInt(s3[0].substring(i, i + 1));
					}
					for (int i = 0; i < s3[1].length(); i++) {
						this.naissance[i] = Integer.parseInt(s3[1].substring(i, i + 1));
					}
				} else {
					throw new PatternSyntaxException("Ligne #R incorecte pour fichier:" + this.f.getAbsolutePath(), s2, -1);
				}
			} else if (s2.startsWith("#S") || s2.startsWith("#T")) {
				if (s2.matches("#S\\s\\d+\\s\\d+\\s\\d+\\s\\d+")) {
					this.type = 1;
					String s3[] = s2.substring(3).split(" ");
					this.limites = new int[4];
					this.limites[0] = Integer.parseInt(s3[0]);
					this.limites[1] = Integer.parseInt(s3[1]);
					this.limites[2] = Integer.parseInt(s3[2]);
					this.limites[3] = Integer.parseInt(s3[3]);
				} else if (s2.matches("#T\\s\\d+\\s\\d+\\s\\d+\\s\\d+")) {
					this.type = 8;
					String s3[] = s2.substring(3).split(" ");
					this.limites = new int[4];
					this.limites[0] = Integer.parseInt(s3[0]);
					this.limites[1] = Integer.parseInt(s3[1]);
					this.limites[2] = Integer.parseInt(s3[2]);
					this.limites[3] = Integer.parseInt(s3[3]);
				}

			} else {
				this.s = s + s2 + "\n";
			}
		}
		this.br.close();
		this.ipsr.close();
		this.ips.close();
	}

	/**
	 * Renvoie une chaîne donnant la position et le placement des Cellules
	 * 
	 * @return Une chaîne donnant la position et le placement des Cellules
	 */
	public String getS() {
		return this.s;
	}

	/**
	 * Renvoie un tableau contenant les règles de survie de ce monde
	 * 
	 * @return Un tableau contenant les règles de survie de ce monde
	 */
	public int[] getSurvie() {
		return survie;
	}

	/**
	 * Renvoie un tableau contenant les règles de naissance de ce monde
	 * 
	 * @return Un tableau contenant les règles de naissance de ce monde
	 */
	public int[] getNaissance() {
		return naissance;
	}

	/**
	 * Renvoie un tableau contenant les limites de ce monde s'il y en a
	 * 
	 * @return Un tableau contenant les limites
	 */
	public int[] getLimites() {
		return limites;
	}

	/**
	 * Renvoie le type de ce monde 0 pour un monde illimité 1 pour un monde
	 * limité et 8 pour un monde circulaire
	 * 
	 * @return Le type de ce monde
	 */
	public int getType() {
		return type;
	}

}
