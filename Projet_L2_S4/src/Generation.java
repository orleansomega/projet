
/**
 * <p>Classe de représentant une génération du jeu de la vie</p> 
 * <p>Implémente Comparable</p>
 * 
 * @author LECOMTE Rodolphe
 *
 */
public class Generation implements Comparable<Generation> {
	private ListeTrie<Cellule> lt;
	private int num;

	/**
	 * Créer une nouvelle génération
	 * 
	 * @param lt
	 *            Une liste contenant toute les Cellule de cette Génération
	 * @param num
	 *            Le numéro de la génération
	 */
	public Generation(ListeTrie<Cellule> lt, int num) {
		this.lt = lt;
		this.num = num;
	}

	/**
	 * Renvoie la liste de cette génération
	 * 
	 * @return La liste de cette génération
	 */
	public ListeTrie<Cellule> getLt() {
		return lt;
	}

	/**
	 * Renvoie le numero de la génération
	 * 
	 * @return Le numéro de la génération
	 */
	public int getNum() {
		return num;
	}

	/**
	 * Test si la liste est morte
	 * 
	 * @return Si la liste est vide cette génération est morte renvoie true dans
	 *         ce cas et false sinon
	 */
	public boolean estMort() {
		return this.lt.estVide();
	}

	/**
	 * Calcule la génération suivante selon les régles passées en paramétre et
	 * la renvoie
	 * 
	 * @param survie
	 *            Tableau contenant les régles de survie d'une Cellule
	 * @param naissance
	 *            Tableau contenant les régles de naissance d'une Cellule
	 * @param circulaire
	 *            Si le monde est circulaire
	 * @param ferme
	 *            Si le monde est fermé
	 * @param limites
	 *            Si le monde est circulaire ou fermé contient ses limites
	 * @return La génération suivante
	 */
	public Generation nextGeneration(int[] survie, int[] naissance, boolean circulaire, boolean ferme, int[] limites) {
		if (!lt.estVide()) {
			ListeTrie<Cellule> lt2 = lt.clone();
			ListeTrie<CelluleDeSchrodinger> ltTemp = lt2.pop().listeCellulesDeSchrodinger(circulaire, ferme, limites);
			while (!lt2.estVide()) {
				ltTemp.add(lt2.pop().listeCellulesDeSchrodinger(circulaire, ferme, limites));
			}
			while (!ltTemp.estVide()) {
				CelluleDeSchrodinger cq = ltTemp.pop();
				if (cq.calculerExistance(survie, naissance)) {
					lt2.add(cq.creerCellule());
				}
			}
			return new Generation(lt2, this.num + 1);
		} else {
			return new Generation(lt.clone(), this.num + 1);
		}
	}

	/**
	 * Renvoie une ListeTrie de Cellule dont la premiére Cellule a étée mis a la
	 * position 0,0 et toutes les autres placées dans la position qui correspond
	 * 
	 * @return Une ListeTrie de Cellule
	 */
	public ListeTrie<Cellule> toZeroListe() {
		ListeTrie<Cellule> lt1 = new ListeTrie<Cellule>();
		ListeTrie<Cellule> ltTemp = this.lt.clone();
		int positionx = this.lt.getPremier().getPosx();
		int positiony = this.lt.getPremier().getPosy();
		while (!ltTemp.estVide()) {
			Cellule c = new Cellule(ltTemp.getPremier().getPosx() - positionx,
					ltTemp.getPremier().getPosy() - positiony);
			lt1.add(c);
			ltTemp = ltTemp.queue();
		}

		return lt1;
	}

	/**
	 * Compare si les deux objets ont le même numéro de génération
	 * 
	 * @param o
	 *            La génération a comparer
	 * @return Renvoie true si les deux génération ont le même numéro
	 */
	public boolean equals(Object o) {
		Generation g = (Generation) o;
		return this.num == g.num;
	}

	/**
	 * Compare si les liste de Cellules des deux générations sont identiques
	 * 
	 * @param g
	 *            La génération dont ont va comparer les Cellules
	 * @return Renvoie true si les deux liste sont identiques false sinon
	 */
	public boolean compareGeneration(Generation g) {
		return this.lt.equals(g.getLt());
	}

	/**
	 * Compare si le numéro de cette Generation est inférieur, supérieur ou
	 * égale a la Generation g
	 * 
	 * @param g
	 *            Generation a comparer
	 * @return <p>-1 si le numéro de cette Generation est inférieur a la Generation
	 *         c</p>
	 *         <p>0 si elle sont égales</p>
	 *         <p>1 si le numéro de cette Generation est supérieur</p>
	 */
	@Override
	public int compareTo(Generation g) {
		if (this.getNum() < g.getNum()) {
			return -1;
		} else if (this.getNum() > g.getNum()) {
			return 1;
		}
		return 0;
	}

}
