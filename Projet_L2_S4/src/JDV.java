import java.io.File;
import java.io.IOException;
import java.util.regex.PatternSyntaxException;

/**
 * Classe assurant la lecture de fichier LIF, la création des génération
 * successives et la recherche du type des schémas.
 * 
 * @author NAIL Theo
 */
public class JDV {

	/**
	 * Fonction principale du programme fait appel a toutes les autres classes
	 * et fonctions
	 * 
	 * @param args
	 *            Tableau contenant les argumeants passé en paramêtres au
	 *            programme
	 */
	public static void main(String[] args) {
		if (args.length == 1) {
			if (args[0].equals("-name")) {
				System.out.println("Ce programme a été développé par : NAIL Theo\nLECOMTE Rodolphe");
			} else if (args[0].equals("-h")) {
				System.out.println("" + "java -jar JeuDeLaVie.jar -name affiche vos noms et prénoms\n"
						+ "java -jar JeuDeLaVie.jar -h rappelle la liste des options du programme\n"
						+ "java -jar JeuDeLaVie.jar -s d fichier.lif exécute une simulation du jeu d’une durée d affichant les configurations du jeu avec le numéro de génération.\n"
						+ "java -jar JeuDeLaVie.jar -c max fichier.lif calcule le type d’évolution du jeu avec ses caractéristiques (taille de la queue, période et déplacement); max représente la durée maximale de simulation pour déduire les résultats du calcul.\n"
						+ "java -jar JeuDeLaVie.jar -w max dossier calcule le type d’évolution de tous les jeux contenus dans le dossier passé en paramètre et affiche les résultats sous la forme d’un fichier html.");
			} else {
				System.out.println("Paramétres incorecte");
			}
		}

		else if (args.length == 3) {
			System.out.println(args[0] + "," + args[1] + "," + args[2]);
			if (args[0].equals("-s")) {
				if (Core.isInt(args[1])) {
					if (args[2] instanceof String) {
						File f;
						if ((f = new File(args[2])).exists()) {
							try {
								LecteurLif ll = new LecteurLif(f);
								Plateau p = new Plateau(ll.getS(), ll.getSurvie(), ll.getNaissance(), ll.getType(),
										ll.getLimites());
								Core.Render(p, Integer.parseInt(args[1]));
							} catch (PatternSyntaxException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							}
						} else {
							System.out.println("Ce fichier n'existe pas");
						}
					} else {
						System.out.println("Le troisieme argument doit etre un String");
					}
				} else {
					System.out.println("Le second argument doit etre un nombre entier");
				}
			}

			else if (args[0].equals("-c")) {
				if (Core.isInt(args[1])) {
					if (args[2] instanceof String) {
						File f;
						if ((f = new File(args[2])).exists()) {
							try {
								LecteurLif ll = new LecteurLif(f);
								Plateau p = new Plateau(ll.getS(), ll.getSurvie(), ll.getNaissance(), ll.getType(),
										ll.getLimites());
								System.out.println(p.getType(50));
							} catch (PatternSyntaxException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							}
						} else {
							System.out.println("Ce fichier n'existe pas");
						}
					} else {
						System.out.println("Le troisieme argument doit etre un String");
					}
				} else {
					System.out.println("Le second argument doit etre un nombre entier");
				}
			}

			else if (args[0].equals("-w")) {
				if (Core.isInt(args[1])) {
					if (args[2] instanceof String) {
						if (new File(args[2]).exists()) {
							File[] tab = Core.FolderArr(args[2]);
							for (File f : tab) {
								try {
									LecteurLif ll = new LecteurLif(f);
									Plateau p = new Plateau(ll.getS(), ll.getSurvie(), ll.getNaissance(), ll.getType(),
											ll.getLimites());
									System.out.println(f.getName() + "\n" + p.getType(50));
								} catch (PatternSyntaxException e) {
									e.printStackTrace();
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
						} else {
							System.out.println("Dossier inexistant!");
						}
					} else {
						System.out.println("Cet argument doit etre une chaine de charactere!");
					}
				} else {
					System.out.println("Le second argument doit etre un nombre entier");
				}

			}

			else {
				System.out.println("Incorect parameters");
			}
		}
	}

}
