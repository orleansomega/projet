
/**
 * <p>Cellule utilisé lors du calcule de la génération suivante elle poséde des
 * coordonnées x, y ainsi qu'un état (vivant ou mort) et un nombre de
 * voisins</p>
 * <p>Implémente Comparable</p>
 * 
 * @author LECOMTE Rodolphe
 *
 */
public class CelluleDeSchrodinger implements Comparable<CelluleDeSchrodinger> {
	private int posx;
	private int posy;
	private boolean estVivante;
	private int nbVoisins;

	/**
	 * Créer une CelluleDeSchrodinger avec les coordonnées et l'état indiqué
	 * 
	 * @param PositionX
	 *            Position X de la Cellule
	 * @param PositionY
	 *            Position Y de la Cellule
	 * @param EstVivante
	 *            <p>Etat de la Cellule</p>
	 *            <p>Si la Cellule n'est pas vivante le nombre de voisin passe de 0
	 *            a 1</p>
	 */
	public CelluleDeSchrodinger(int PositionX, int PositionY, boolean EstVivante) {
		this.posx = PositionX;
		this.posy = PositionY;
		this.estVivante = EstVivante;

		if (this.estVivante) {
			this.nbVoisins = 0;
		} else {
			this.nbVoisins = 1;
		}
	}

	public int getPosx() {
		return posx;
	}

	/**
	 * Renvoie la position Y de cette Cellule
	 * 
	 * @return La position Y de cette Cellule
	 */
	public int getPosy() {
		return posy;
	}

	/**
	 * Renvoie l'état de cette Cellule
	 * 
	 * @return L'état de cette Cellule
	 */
	private boolean getEstVivante() {
		return this.estVivante;
	}

	/**
	 * Créer une Cellule avec les même coordonnées que cette Cellule
	 * 
	 * @return Une Cellule avec les même coordonnées que cette Cellule
	 */
	public Cellule creerCellule() {
		return new Cellule(this.posx, this.posy);
	}

	/**
	 * Renvoie l'état de cette Cellule suivante les deux tableau passé en
	 * paramétre
	 * 
	 * @param survie
	 *            Un tableau d'entier donnant le nombre de voisins nécéssaire
	 *            pour que cette Cellule reste en vie (si elle était déja
	 *            vivante)
	 * @param naissance
	 *            Un tableau d'entier donnant le nombre de voisins nécéssaire
	 *            pour que cette Cellule naise (si elle n'était pas déja
	 *            vivante)
	 * @return Renvoie true si cette cellule devrait être vivante selon les deux
	 *         tableau passés en paramétre
	 */
	public boolean calculerExistance(int[] survie, int[] naissance) {
		if (this.estVivante) {
			return this.estDans(this.nbVoisins, survie);
		} else {
			return this.estDans(this.nbVoisins, naissance);
		}
	}

	/**
	 * Test si l'entier est dans le tableau d'entier
	 * 
	 * @param a
	 *            Entier a tester
	 * @param tab
	 *            Tableau a tester
	 * @return Renvoie true si l'entier est dans et false si il n'y est pas ou
	 *         si le tableau est vide
	 */
	public boolean estDans(int a, int[] tab) {
		boolean b = false;
		if (tab == null) {

		} else {
			for (int i = 0; i < tab.length; i++) {
				b = b || (a == tab[i]);
			}
		}
		return b;
	}

	/**
	 * Compare si cette Cellule est inférieure, supérieure ou égale a la Cellule
	 * c
	 * 
	 * @param c
	 *            Cellule a comparer
	 * @return <p>-1 si cette Cellule est inférieure a la Cellule c</p>
	 *         <p>0 si elle sont égales</p>
	 *         <p>1 si cette Cellule est supérieure</p>
	 */
	@Override
	public int compareTo(CelluleDeSchrodinger c) {
		if (this.getPosx() < c.getPosx() || (this.getPosx() == c.getPosx() && this.getPosy() < c.getPosy())) {
			return -1;
		} else if (this.getPosx() == c.getPosx() && this.getPosy() == c.getPosy()) {
			if (c.getEstVivante() && (!this.getEstVivante())) {
				this.estVivante = true;
			} else {
				this.nbVoisins++;
			}
			return 0;
		} else {
			return 1;
		}
	}

	/**
	 * Compare si les deux Cellules sont identiques
	 * 
	 * @param c
	 *            Cellule a comparer
	 * @return Renvoie true si les positions des deux cellules sont les même et
	 *         false sinon
	 */
	public boolean equals(CelluleDeSchrodinger c) {
		return this.posx == c.posx && this.posy == c.posy;
	}

	/**
	 * Compare si les deux Cellules sont identiques
	 * 
	 * @param o
	 *            Cellule a comparer (après cast)
	 * @return Renvoie true si les positions des deux cellules sont les même et
	 *         false sinon
	 */
	public boolean equals(Object o) {
		CelluleDeSchrodinger c = (CelluleDeSchrodinger) o;
		return ((this.posx == c.posx) && (this.posy == c.posy));
	}

	/**
	 * Renvoie une chaîne de caractére décrivant cette Cellule
	 * 
	 * @return Une chaîne de caractére sous la forme (Vivante: true ou false,
	 *         positionX, positionY, nbvoisins:nbvoisins)
	 */
	public String toString() {
		return "(Vivante:" + this.estVivante + ", " + this.posx + ", " + this.posy + ", nbvoisins:" + this.nbVoisins
				+ ")";
	}

}
