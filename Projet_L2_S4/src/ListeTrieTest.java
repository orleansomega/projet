import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Classe de test pour la ListeTrie
 * @author BONCOUR Fabien
 *
 */
public class ListeTrieTest {

	@BeforeClass
	public static void setUpClass() throws Exception {
		System.out.println("Début des tests");
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
		System.out.println("Fin des tests");
	}
	
	@Before
	public void setUp() throws Exception {}

	@After
	public void tearDown() throws Exception {
		System.out.println("Test OK");
	}

	@Test
	public void testListeTrie() {
		System.out.println("Test création liste vide");
		ListeTrie<Integer> lt=new ListeTrie<Integer>();
		assertNotNull(lt);
	}

	@Test
	public void testListeTrieT() {
		System.out.println("Test création liste avec un entier 1");
		ListeTrie<Integer> lt=new ListeTrie<Integer>(1);
		assertNotNull(lt);
	}

	@Test
	public void testGetPremier() {
		System.out.println("Test recupération premier élément");
		ListeTrie<Integer> lt=new ListeTrie<Integer>(1);
		assertNotNull(lt);
		assertNotNull(lt.getPremier());
		assertEquals(1, lt.getPremier().intValue());
	}

	@Test
	public void testGetDernier() {
		System.out.println("Test recupération dernier élément");
		ListeTrie<Integer> lt=new ListeTrie<Integer>(1);
		lt.add(2);
		assertNotNull(lt);
		assertNotNull(lt.getDernier());
		assertEquals(2, lt.getDernier().intValue());
	}

	@Test
	public void testQueue() {
		System.out.println("Test queue on enleve le premier élément(1) il ne reste que 2 et 3 qui doivent être en première et dernière position respectivenment");
		ListeTrie<Integer> lt=new ListeTrie<Integer>(1);
		lt.add(3);
		lt.add(2);
		lt=lt.queue();
		assertNotNull(lt);
		assertEquals(2, lt.getPremier().intValue());
		assertEquals(3, lt.getDernier().intValue());
		lt=lt.queue();
		assertNotNull(lt);
		assertEquals(3, lt.getPremier().intValue());
		assertEquals(3, lt.getDernier().intValue());
		lt=lt.queue();
		assertNotNull(lt);
		assertEquals(null, lt.getPremier());
	}

	@Test
	public void testAddT() {
		System.out.println("Test d'ajouts d'élément dans l'ordre on genére une liste vide puis l'on ajoute 2, 1, 2 et 3 (on ajoute deux fois 2 pour tester que l'on n'ajoute pas deux fois le même élément)");
		ListeTrie<Integer> lt=new ListeTrie<Integer>();
		lt.add(2);
		lt.add(1);
		lt.add(2);
		lt.add(3);
		assertEquals(1, lt.getPremier().intValue());
		assertEquals(3, lt.getDernier().intValue());
		assertTrue(lt.toString().equals("[1,2,3]"));
		System.out.println("Résultat a obtenir : [1,2,3]");
		System.out.println("Résultat obtenu : "+lt.toString());
	}

	@Test
	public void testAddListeTrieOfT() {
		System.out.println("Test d'ajout d'une liste a celle-ci");
		ListeTrie<Integer> lt=new ListeTrie<Integer>(1);
		lt.add(5);
		lt.add(3);
		ListeTrie<Integer> lt1=new ListeTrie<Integer>(0);
		lt1.add(2);
		lt1.add(4);
		lt1.add(6);
		lt.add(lt1);
		assertEquals(0, lt.getPremier().intValue());
		assertEquals(6, lt.getDernier().intValue());
		assertTrue(lt.toString().equals("[0,1,2,3,4,5,6]"));
	}

	@Test
	public void testPop() {
		System.out.println("Test queue on enleve le premier élément(1) il ne reste que 2 et 3 qui doivent être en première et dernière position respectivenment");
		ListeTrie<Integer> lt=new ListeTrie<Integer>(1);
		lt.add(3);
		lt.add(2);
		assertNotNull(lt);
		assertEquals(1,lt.pop().intValue());
		assertEquals(2, lt.getPremier().intValue());
		assertEquals(3, lt.getDernier().intValue());
	}

	@Test
	public void testClone() {
		System.out.println("Test clone");
		ListeTrie<Integer> lt=new ListeTrie<Integer>(1);
		lt.add(3);
		lt.add(2);
		ListeTrie<Integer> lt1=lt.clone();
		assertNotNull(lt1);
		assertEquals(1, lt1.getPremier().intValue());
		assertEquals(3, lt1.getDernier().intValue());
	}

	@Test
	public void testEstVide() {
		System.out.println("Test estVide");
		ListeTrie<Integer> lt=new ListeTrie<Integer>();
		assertNull(lt.getPremier());
		assertTrue(lt.estVide());
		lt.add(3);
		lt.add(2);
		assertNotNull(lt.getPremier());
		assertFalse(lt.estVide());
	}

	@Test
	public void testLength() {
		System.out.println("Test length");
		ListeTrie<Integer> lt=new ListeTrie<Integer>();
		assertEquals(0, lt.length());
		lt.add(3);
		assertEquals(1, lt.length());
		lt.add(2);
		assertEquals(2, lt.length());
	}

	@Test
	public void testEqualsListeTrieOfT() {
		System.out.println("Test d'egalité");
		ListeTrie<Integer> lt=new ListeTrie<Integer>(1);
		lt.add(5);
		lt.add(3);
		ListeTrie<Integer> lt1=new ListeTrie<Integer>(1);
		lt1.add(5);
		lt1.add(3);
		ListeTrie<Integer> lt2=new ListeTrie<Integer>(2);
		lt2.add(3);
		lt2.add(4);
		assertTrue(lt.equals(lt1));
		assertTrue(lt1.equals(lt));
		assertFalse(lt.equals(lt2));
		assertFalse(lt2.equals(lt));
	}

	@Test
	public void testToString() {
		System.out.println("Test du toString");
		ListeTrie<Integer> lt=new ListeTrie<Integer>(1);
		lt.add(5);
		lt.add(3);
		assertTrue(lt.toString().equals("[1,3,5]"));
	}

}
