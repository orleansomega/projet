
/**
 * <p>Classe représentant une cellule vivante </p>
 * Implémente Comparable
 * 
 * @author LECOMTE Rodolphe
 *
 */
public class Cellule implements Comparable<Cellule> {
	private int posx;
	private int posy;

	/**
	 * Créer une Cellule ayant des coordonnées X et Y
	 * 
	 * @param PositionX
	 *            La position de la Cellule en X
	 * @param PositionY
	 *            La position de la Cellule en Y
	 */
	public Cellule(int PositionX, int PositionY) {
		this.posx = PositionX;
		this.posy = PositionY;
	}

	/**
	 * Renvoie la position en X de cette Cellule
	 * 
	 * @return La position en X de cette Cellule
	 */
	public int getPosx() {
		return posx;
	}

	/**
	 * Renvoie la possition en Y de cette Cellule
	 * 
	 * @return La position en Y de cette Cellule
	 */
	public int getPosy() {
		return posy;
	}

	/**
	 * <p>Créer une ListeTrie de CelluleDeSchrodinger qui contient cette cellule et
	 * les huit CelluleDeSchrodinger adjascentes si le monde est illimité. </p>
	 * <p>Si le monde est limité et que l'on a atteint l'une (ou deux) des limites
	 * on ne créer pas les CelluleDeSchrodinger concernées (C'est-a-dire que les
	 * CelluleDeSchrodinger dépassant des limites ne sont pas créées.</p>
	 * <p>Si le monde est circulaire et que l'on a atteint l'une (ou deux) des
	 * limites on crée les CelluleDeSchrodinger concernés mais en ajustant leurs
	 * positions a l'autre bout du plateau</p>
	 * 
	 * @param circulaire
	 *            Si le monde est circulaire
	 * @param ferme
	 *            Si le monde est fermé
	 * @param limites
	 *            Dans le cas ou le monde est circulaire ou fermé quelle sont
	 *            ses limites
	 * @return Une ListeTrie de CelluleDeSchrodinger.
	 */
	public ListeTrie<CelluleDeSchrodinger> listeCellulesDeSchrodinger(boolean circulaire, boolean ferme,
			int[] limites) {
		ListeTrie<CelluleDeSchrodinger> lt = new ListeTrie<CelluleDeSchrodinger>();
		for (int i = 1; i > -2; i--) {
			for (int j = 1; j > -2; j--) {
				if (i == 0 && j == 0) {
					lt.add(new CelluleDeSchrodinger(this.posx + j, this.posy + i, true));
				} else {
					if (ferme && (this.posx + j < limites[0] || this.posx + j > limites[1] || this.posy + i < limites[2]
							|| this.posy + i > limites[3])) {
					} else if (circulaire && (this.posx + j < limites[0] || this.posx + j > limites[1]
							|| this.posy + i < limites[2] || this.posy + i > limites[3])) {
						int correcteurX = 0;
						int correcteurY = 0;
						if (this.posx + j < limites[0]) {
							correcteurX = (limites[1] - limites[0]) + 1;
						}
						if (this.posx + j > limites[1]) {
							correcteurX = (limites[0] - limites[1]) - 1;
						}
						if (this.posy + i < limites[2]) {
							correcteurY = (limites[3] - limites[2]) + 1;
						}
						if (this.posy + i > limites[3]) {
							correcteurY = (limites[2] - limites[3]) - 1;
						}

						lt.add(new CelluleDeSchrodinger(this.posx + j + correcteurX, this.posy + i + correcteurY,
								false));

					} else {
						lt.add(new CelluleDeSchrodinger(this.posx + j, this.posy + i, false));
					}
				}
			}
		}
		return lt;
	}

	/**
	 * Test si les deux Cellules sont identiques
	 * 
	 * @param c
	 *            La cellule a tester
	 * @return Retourne true si la position des deux cellules est la même
	 */
	public boolean equals(Cellule c) {
		return (this.posx == c.posx) && (this.posy == c.posy);
	}

	/**
	 * Test si les deux Cellules sont identiques (utiliser par la ListeTrie)
	 * 
	 * @param o
	 *            La cellule a tester
	 * @return Retourne true si la position des deux cellules est la même
	 */
	public boolean equals(Object o) {
		Cellule c = (Cellule) o;
		return (this.posx == c.posx) && (this.posy == c.posy);
	}

	/**
	 * Compare les deux Cellules (d'abord la position x puis la position y si
	 * les positions x sont identiques)
	 * 
	 * @param c
	 *            La Cellule a comparer
	 * @return <p>-1 si cette Cellule est inférieure a la Cellule c</p>
	 *         <p>0 si les deux sont égales</p>
	 *         <p>1 si cette Cellule est supérieure a la Cellule c</p>
	 */
	@Override
	public int compareTo(Cellule c) {
		if (this.posx < c.posx || (this.posx == c.posx && this.posy < c.posy)) {
			return -1;
		} else if (this.posx == c.posx && this.posy == c.posy) {
			return 0;
		} else {
			return 1;
		}
	}

	/**
	 * Renvoie une chaîne de caractére décrivant cette Cellule
	 * 
	 * @return Une chaîne de caractére sous la forme (positionX,positionY)
	 */
	public String toString() {
		return "(" + this.posx + "," + this.posy + ")";
	}

}
