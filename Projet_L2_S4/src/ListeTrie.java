
/**
 * Cette classe est une liste triée, générique simplement chaîné
 * 
 * @author BONCOUR Fabien
 *
 * @param <T>
 *            Type de la liste generique ce type doit implémenter Comparable
 */
public class ListeTrie<T extends Comparable<T>> {
	private Maillon<T> tete;
	private Maillon<T> fin;

	/**
	 * Créer une liste vide
	 */
	public ListeTrie() {
		this.tete = null;
		this.fin = null;
	}

	/**
	 * Créer une liste contenant un seul élément de type T
	 * 
	 * @param Element
	 *            Le premier élément que cette liste va contenir
	 */
	public ListeTrie(T Element) {
		this.tete = new Maillon<T>(Element, null);
		this.fin = this.tete;
	}

	/**
	 * Créer un clone de cette liste mais pas de ses éléments
	 * 
	 * @param debut
	 *            Le début de la liste
	 * @param fin
	 *            La fin de la liste
	 */
	private ListeTrie(Maillon<T> debut, Maillon<T> fin) {
		this.tete = debut;
		this.fin = fin;
	}

	/**
	 * Renvoie le premier élément de cette liste
	 * 
	 * @return Le premier élément de la liste et si elle est vide renvoie null
	 */
	public T getPremier() {
		if (this.tete == null) {
			return null;
		}
		return this.tete.getElem();
	}

	/**
	 * Renvoie le dernier élément de cette liste
	 * 
	 * @return Le dernier élément de la liste et si elle est vide renvoie null
	 */
	public T getDernier() {
		if (this.fin == null) {
			return null;
		}
		return this.fin.getElem();
	}

	/**
	 * Renvoie une liste sans le premier élément
	 * 
	 * @return Une nouvelle liste qui ne contient pas le premier élément.
	 */
	public ListeTrie<T> queue() {
		if (this.tete == null) {
			return this;
		}
		if (this.tete.getSuiv() == null) {
			this.fin = null;
		}
		return new ListeTrie<T>(tete.getSuiv(),this.fin);
	}

	/**
	 * Ajoute l'élément a la liste en utilisant son compareTo pour le placer a
	 * la bonne place
	 * 
	 * @param Element
	 *            L'élément a rajouter
	 */
	public void add(T Element) {
		if (this.tete == null) {
			this.tete = new Maillon<T>(Element, null);
			this.fin = this.tete;
		} else {
			Maillon<T> m1 = this.tete;
			int a = m1.getElem().compareTo(Element);
			if (a > 0) {
				this.tete = new Maillon<T>(Element, m1);
			} else {
				Maillon<T> m2 = this.tete.getSuiv();
				while ((m2 != null) && (a != 0) && ((a = m2.getElem().compareTo(Element)) < 0)) {
					m1 = m2;
					m2 = m2.getSuiv();
				}
				if (m2 != null && a == 0) {

				} else {
					Maillon<T> mTemp = new Maillon<T>(Element, m2);
					m1.setSuiv(mTemp);
					if (m2 == null) {
						this.fin = mTemp;
					}
				}
			}
		}
	}

	/**
	 * Permet de fusionner deux ListeTrie ensemble
	 * 
	 * @param lt
	 *            La liste trie que l'on ajoute a celle-ci
	 */
	public void add(ListeTrie<T> lt) {
		while (!lt.estVide()) {
			this.add(lt.getPremier());
			lt = lt.queue();
		}
	}

	/**
	 * Renvoie la premier élément et l'enléve de la liste
	 * 
	 * @return Le premier élément de cette liste
	 */
	public T pop() {
		if (this.tete == null) {
			return null;
		}
		if (this.tete.getSuiv() == null) {
			this.fin = null;
		}
		T a = this.tete.getElem();
		this.tete = this.tete.getSuiv();
		return a;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	public ListeTrie<T> clone() {
		return new ListeTrie<T>(this.tete, this.fin);
	}

	/**
	 * Renvoie si la liste est vide ou non
	 * 
	 * @return Renvoie true si la liste est vide false sinon
	 */
	public boolean estVide() {
		if (this.tete == null) {
			return true;
		}
		return false;
	}

	/**
	 * Renvoie la longueur de cette liste
	 * 
	 * @return La taille de la liste
	 */
	public int length() {
		if (this.tete == null) {
			return 0;
		}
		Maillon<T> m = this.tete;
		int a = 1;
		while (m.getSuiv() != null) {
			m = m.getSuiv();
			a++;
		}
		return a;
	}

	/**
	 * Test si deux liste et leurs contenu sont égale entre elles
	 * 
	 * @param l
	 *            La liste a tester
	 * @return Renvoie true si les deux liste sont égales false sinon
	 */
	public boolean equals(ListeTrie<T> l) {
		ListeTrie<T> l1 = this;
		ListeTrie<T> l2 = l;
		while ((l1.getPremier() != null && l2.getPremier() != null) && l1.getPremier().equals(l2.getPremier())) {
			l1 = l1.queue();
			l2 = l2.queue();
		}
		if (l1.getPremier() == null && l2.getPremier() == null) {
			return true;
		}
		return false;
	}

	/**
	 * Renvoie une chaîne de caracters decrivant cette liste et ses éléments
	 * (fait appel au toString de ces derniers)
	 * 
	 * @return Une chaîne de caracters sous la forme
	 *         [Element1.toString,Element2.toString,...,ElementN.toString]
	 */
	public String toString() {
		String s = "[";
		if (this.tete == null) {
			s = "Vide";
		} else {
			Maillon<T> m = this.tete;
			while (m != null) {
				s = s + m.getElem().toString() + ",";
				m = m.getSuiv();
			}
			s = s.substring(0, s.length() - 1) + "]";
		}
		return s;
	}

	/**
	 * Classe interne a la classe ListeTrie Sert de Maillon
	 * 
	 * @author LECOMTE Rodolphe
	 *
	 * @param <T>
	 *            Type de l'élément contenu dans le maillon ce dernier doit
	 *            implémenter Comparable
	 */
	private class Maillon<T> {
		private T elem;
		private Maillon<T> suiv;

		/**
		 * Créer un maillon contenant un élément et un pointeur sur le Maillon
		 * suivant
		 * 
		 * @param Element
		 *            L'élément de type T a stocker dans ce Maillon
		 * @param Suivant
		 *            Un pointeur sur le Maillon suivant
		 */
		private Maillon(T Element, Maillon<T> Suivant) {
			this.elem = Element;
			this.suiv = Suivant;
		}

		/**
		 * Renvoie l'élément contenu dans ce Maillon
		 * 
		 * @return L'élément de ce Maillon
		 */
		private T getElem() {
			return elem;
		}

		/**
		 * Remplace l'élément actuelle par cet Element
		 * 
		 * @param Element
		 *            Le nouvelle élément
		 */
		private void setElem(T Element) {
			this.elem = Element;
		}

		/**
		 * Renvoie le Maillon suivant
		 * 
		 * @return Le pointeur vers le Maillon suivant
		 */
		private Maillon<T> getSuiv() {
			return suiv;
		}

		/**
		 * Permet de remplacer le pointeur sur le Maillon suivant par un autre
		 * 
		 * @param suiv
		 *            Le nouveau pointeur
		 */
		private void setSuiv(Maillon<T> suiv) {
			this.suiv = suiv;
		}

	}
}
